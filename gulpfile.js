var gulp = require('gulp');
var node_modules = './node_modules/'
gulp.task('default', function () {
    var index = gulp.src('./index.html')
        .pipe(gulp.dest('./dist/'));
    var tinymce = gulp.src([
            node_modules + 'tinymce/**',
        ]).pipe(gulp.dest('./dist/js/tinymce/'));
    var uikit = gulp.src([
            node_modules + 'uikit/**'
        ]).pipe(gulp.dest('./dist/vendor/uikit/'));
    var assets = gulp.src([
            './src/assets/images/**'
        ]).pipe(gulp.dest('./dist/images/'));
    var js = gulp.src([
            './src/assets/js/**'
        ]).pipe(gulp.dest('./dist/js/'));
    var css = gulp.src([
            './src/assets/css/**'
        ]).pipe(gulp.dest('./dist/css/'));
    return {}
})