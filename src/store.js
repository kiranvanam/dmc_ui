import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

import ack from './components/common/ack/store'
import misc from './components/common/vuex/misc.js'
import currentConfig from './components/common/vuex/config.js'
import auth from './components/auth/store/store'
import search from './components/book/store/store.js'
//modules = $.extend(modules, searchRelated)

export default new Vuex.Store({
    strict:true,
    modules: {
        misc,
        currentConfig,
        ack,
        auth,
        search
    }
})
