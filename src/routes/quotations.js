export default [
    {
        path: '/dashboard/book/hotels/search',
        component:require('../components/book/hotel-search.vue')
    },
    {
        path: '/dashboard/book/hotels',
        component:require('../components/book/hotels.vue')
    }
]
