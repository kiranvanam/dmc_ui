var routesall = {
  home : require('./home.js'),
  quotations: require('./quotations.js')
};
var routes = [];
for(let module in routesall){
  const tmp = routesall[module];
  for (let i in tmp) {
    routes = routes.concat(tmp[i]);
  }
}

import authRoutes from '../components/auth/routes.js'
const UrlsWithoutAuthenticationRequired = ['/register','/login', '/password-reset']

function isAuthenticationUrl(route) {
  return _.find(UrlsWithoutAuthenticationRequired, (url) => { return url == route})
}

function isAuthenticationRequired(route, index, array) {
  //console.log(route, UrlsWithoutAuthenticationRequired)
  return  isAuthenticationUrl(route) == null
}

import store from '../store.js'
export default [
  {
    path: '/',
    beforeEnter:(to, from , next) => {
       next({name:'admin-home'})
    }
  },
  {
    path : '/auth',
    component: require('../views/auth/home.vue'),
    children: authRoutes
  },
  {
    path : '/admin',
    name :'admin-home',
    title : 'Admin page',
    component: require('../views/dashboard/home.vue'),
    beforeEnter: (to, from, next) => {
                      document.title = "Home page title"
                      if(isAuthenticationRequired(to.path)) {
                        if(!store.state.auth.auth.isAuthenticated()) {
                          next({name:'login'})
                        }
                      }
                      next()
                  },
    children: routes
  }
];
