export default {
	state: {
		moduleName: 'mainContent'
	},
	mutations: {
		SET_CURRENT_MODULE_NAME (state, moduleName) {
			state.moduleName = moduleName;
		}
	},
	actions: {
		setCurrentModuleName: ({commit}, moduleName) => {
			commit('SET_CURRENT_MODULE_NAME', moduleName);
		}
	},
	getters: {
		getCurrentModuleName: (state) => {
			return state.moduleName;
		}
	}
};