
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../../bootstrap');

window.moment = require('moment')

import home from './home.vue'
import store from './store.js'

const dashboard = new Vue({
	store,
	el:'#dashboard',  
	components:{
		home
  	}
});