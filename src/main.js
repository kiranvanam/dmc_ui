import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './views/App.vue'
import axios from 'axios'

import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'

import { DatePicker, TimePicker, TimeSelect } from 'element-ui'

Vue.use(ElementUI, { locale })


// import components
Vue.use(VueRouter);
window.moment = require('moment');
window.axios = axios;
window._ = require('lodash');

// Expose jQuery to the global object
window.jQuery = window.$ = jQuery;

import routes from './routes/index.js'
import store from './store.js'

import editor from './components/common/tinymce-editor.vue'
import spinner from './components/common/utilities/spinner.vue'

Vue.component('editor',editor);
Vue.component('spinner',spinner);

// Element UI Components
Vue.component(DatePicker.name, DatePicker);
Vue.component(TimePicker.name, TimePicker);
Vue.component(TimeSelect.name, TimeSelect);


require('./components/common/helpers/random');

const router = new VueRouter({
	routes,
	mode: 'history'
});

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
