import Vue from 'vue'

// no need to VueResource and rest of functionality, as it;s alredy done
//import  {clearMessages, showErrors, showSuccess, enableAckForComponent, startRequestProcessing, stopRequestProcessing} from '../ack/store';

class Api {
  //let URL = "";// property

  setBaseURL() {
    this.URL = 'http://dmc.dev/admin/api';
  }

  prepareApiURL(url) {
    this.setBaseURL();
    this.URL = this.URL+url;
  }

  preProcessing(dispatch) {
    dispatch('startRequestProcessing');
    dispatch('clearMessages');
  }
  postProcessing( body, ackForComponent, dispatch, ackCallBack, successcb){
    // only for success
    if(successcb)
      successcb(body);

    dispatch(ackCallBack, body);
    dispatch('enableAckForComponent', ackForComponent);
    dispatch('stopRequestProcessing');
  }
  get(urlPath, successcb, dispatch, ackForComponent) {
    this.preProcessing(dispatch);
    axios.get(urlPath)
        .then((response) => {
                this.postProcessing(response.data, ackForComponent, dispatch, 'showSuccess', successcb);
              },
              (response) => {
                this.postProcessing(response.data, ackForComponent, dispatch, 'showErrors');
              }
          );
  }
  fetch(successcb, dispatch, ackForComponent) {
    this.preProcessing(dispatch);
    axios.get(this.URL)
        .then((response) => {
                this.postProcessing(response.data, ackForComponent, dispatch, 'showSuccess', successcb);
              },
              (response) => {
                this.postProcessing(response.data, ackForComponent, dispatch, 'showErrors');
              }
          );
  }
  //to save on specific url
  post(url, resource, successcb, dispatch, ackForComponent) {
    this.preProcessing(dispatch);
    axios.post(url,resource)
            .then(
                (response) => {
                  this.postProcessing(response.data, ackForComponent, dispatch, 'showSuccess', successcb);
                },
                (response) => {
                  this.postProcessing(response.data, ackForComponent, dispatch, 'showErrors');
                }
              );
  }
  save(resource, successcb, dispatch, ackForComponent) {
    this.preProcessing(dispatch);
    axios.post(this.URL,resource)
            .then(
                (response) => {
                  this.postProcessing(response.data, ackForComponent, dispatch, 'showSuccess', successcb);
                },
                (response) => {
                  this.postProcessing(response.data, ackForComponent, dispatch, 'showErrors');
                }
              );
  }

  query(resourceURI, successcb,dispatch, ackForComponent) {
    this.preProcessing(dispatch);
    const url = this.URL + resourceURI;
    axios.get(url)
          .then(
                (response) => {
                  this.postProcessing(response.data, ackForComponent, dispatch, 'showSuccess', successcb);
                },
                (response) => {
                  this.postProcessing(response.data, ackForComponent, dispatch, 'showErrors');
                }
              );
  }

  update(resourceURI, resource, successcb, dispatch, ackForComponent) {
    this.preProcessing(dispatch);
    const url = this.URL + resourceURI;
    axios.put(url,resource)
            .then(
                (response) => {
                  this.postProcessing(response.data, ackForComponent, dispatch, 'showSuccess', successcb);
                },
                (response) => {
                  this.postProcessing(response.data, ackForComponent, dispatch, 'showErrors');
                }
              );
  }

  delete(resourceURI,successcb, dispatch, ackForComponent) {
    this.preProcessing(dispatch);
    const url = this.URL + resourceURI;
    axios.delete(url)
            .then(
                (response) => {
                  this.postProcessing(response.data, ackForComponent, dispatch, 'showSuccess', successcb);
                },
                (response) => {
                  this.postProcessing(response.data, ackForComponent, dispatch, 'showErrors');
                }
              );
  }
}

export default new Api;
