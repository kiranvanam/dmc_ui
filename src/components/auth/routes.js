export default [
    { 
      path: '/', 
      beforeEnter:(to, from , next) => {
         next({name:'login'})
      } 
    },
    { 
      path: 'login',
      name : 'login', 
      component:require('./login.vue') 
    },
    { 
      path: 'register',
      name: 'register',
      component: require('./register.vue')
    },
    {
      path: 'password-reset',
      name : 'password-reset',
      component: require('./password_reset.vue')
    }
]