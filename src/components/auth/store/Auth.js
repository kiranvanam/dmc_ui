export default class {

    setUserDetails(user) {
      this.user = user
    }

    setAuthDetails(auth) {
      this.auth = auth;
    }

    isAuthenticated() {
      return this.auth;
    }

    getPermissions() {
      if(this.auth){
        return this.auth.permissions;
      }
      return [];
    }

    getRoles() {
      if(this.auth) {
        return this.auth.roles
      }
      return [];
    }

    isSuperUser() {
       return _.find(this.getRoles(), (role)=> {
          return role.slug == 'super-user';
       })
    }

    hasPermission(permissionName) {
      if(this.isSuperUser()) {
        return true;
      }
      return _.find(this.permissions(),
                    (permission) => {
                      return permission.slug == permissionName
                    });
    }
}
