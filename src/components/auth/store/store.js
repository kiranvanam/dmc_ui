import Api from '../../core/http-wrapper.js'
import Auth from './Auth.js'

export default {
   state:{
     auth : new Auth
   },
   mutations:{
    SET_ACL_DETAILS(state, details) {
      state.auth.setAuthDetails(details)
    }
   },
   actions:{
      authenticate({commit, state}, { data, router }) {
        axios.post('http://dmc.dev/oauth/token',data)
        .then( (response) => {
          var header =  {
            'Authorization' : 'Bearer ' + response.data.access_token
          }
          axios.get('http://dmc.dev/api/user', { headers: header})
            .then((response)=> {
              commit('SET_ACL_DETAILS',response.data);
               router.push({ name: 'admin-dashboard'});
            })
            .catch((response)=>console.log(response.data))
        })
        .catch((response) => console.log(response.data, "failed"))

      }
   },
   getters:{
    auth(state) {
      return state.auth
    }
   }
}
