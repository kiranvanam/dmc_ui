import Ack from './Ack.js'

export default {
  state :  {
    ack: new Ack,
    isRequestProcessing: false
  },
  mutations : {
    SHOW_ERRORS(state, errors) {
      state.ack.setUp(errors);
    },
    SHOW_SUCCESS(state, success) {
      state.ack.setUp(success);
    },
    CLEAR_ACK(state) {
      state.ack.clear();
    },
    SET_ACK_COMPONENT_NAME(state,name) {
      state.ack.setNameForAckComponent(name);
    },
    START_REQUEST_PROCESSING(state) {
      state.isRequestProcessing = true;
    },
    STOP_REQUEST_PROCESSING(state) {
      state.isRequestProcessing = false
    }
  },
  actions:{
    showErrors : ({commit}, errors) => {
      commit('SHOW_ERRORS', errors)
    },
    showSuccess : ({commit},success) => {
      commit('SHOW_SUCCESS', success)
    },
    clearMessages : ({commit}) => {
      commit('CLEAR_ACK')
    },
    enableAckForComponent: ({commit, dispatch},componentName) =>{
      commit('SET_ACK_COMPONENT_NAME',componentName)
      //hide all ack messages after 1 min ( here time = ms )
      setTimeout( () => dispatch('clearMessages') , 1000*60);
    },
    disableAckForComponent: ({commit}) => {
      commit('SET_ACK_COMPONENT_NAME',"");
    },
    startRequestProcessing : ({commit}) => {
      commit('START_REQUEST_PROCESSING');
    },
    stopRequestProcessing:({commit}) => {
      commit('STOP_REQUEST_PROCESSING');
    }
  },
  getters:{
    ack(state) {
      return state.ack;
    },
    isRequestProcessing(state){
      return state.isRequestProcessing
    }
  }
}
