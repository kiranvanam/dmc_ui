export default class{

 // let formComponentName = "x-creation-form";
 // let response = {
 //             status: 200,
  //            messages : success,
  //            body : {}
  //          };

  getStatus() {
    return this.response.status
  }

  getMessage() {
    return this.response.message;
  }

  getBody() {
    return this.response.body
  }

  setNameForAckComponent(formComponentName) {
    this.formComponentName = formComponentName
  }

  getNameForAckComponent() {
    return this.formComponentName
  }

  isError() {
    return !this.isSuccess();
  }

  isErrorUnknown(){
    return this.response.status == 520
  }

  isSuccess() {
    return this.response.status >= 200 && this.response.status <= 300;
  }

  getErrorMessageFor(field_name) {

    const body = this.getBody();

    if ( body.hasOwnProperty(field_name)) {
      return body[field_name]
    }
    return [];
  }
  getUnKnownErrorDescription() {
    const body = this.getBody();
    return body['description'];
  }

  setUp(response) {
    this.response = response;
  }

  clear() {
    this.response = {};
    this.formComponentName = ""
  }


}
