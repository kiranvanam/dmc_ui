export default {
    state: {
        companyName: 'Go Dubai Tourism',
        facebookURL: 'http://www.facebook.com',
        googlePlusURL: 'http://www.plus.google.com',
        twitterURL: 'http://www.twitter.com'
    },
    getters:{
        currentConfig(state) {
            return {
                companyName: state.companyName,
                facebookURL: state.facebookURL,
                googlePlusURL: state.facebookURL,
                twitterURL: state.twitterURL
            }
        },
    }
}
