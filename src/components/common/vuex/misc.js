import Api from '../../core/http-wrapper.js'
Api.setBaseURL();
let miscApiURL = '/misc';

import moment from 'moment'

export default {
    state: {
        status:[],
        types:[],
        childAges: [],
        mealTypes: [],
        mealPreferences: [],
        currencies:[],
        amenities:[],
        timeOfDay: [],
        tourGuideOptions:[],
        transferPointTypes: [],
        weekDays: moment.weekdays()
    },
    mutations:{
        SET_CHILD_AGES(state, childAges) {
            state.childAges = childAges;
        },
        SET_STATUS(state, status) {
            state.status = status
        },
        SET_CURRENCIES(state, currencies) {
            state.currencies = currencies
        },
        SET_TYPES(state, types) {
            state.types = types
        },
        SET_AMENITIES(state, amenities){
            state.amenities = amenities
        },
        SET_TIME_OF_DAY(state, times) {
            state.timeOfDay  = times
        },
        SET_TOUR_GUIDE_OPTIONS(state, options) {
            state.tourGuideOptions = options
        },
        SET_MEAL_TYPES(state, mealTypes) {
            state.mealTypes = mealTypes;
        },
        SET_MEAL_PREFERENCES(state, mealPreferences) {
            state.mealPreferences = mealPreferences;
        },
        SET_TRANSFER_POINT_TYPES(state, transferPointTypes) {
            state.transferPointTypes = transferPointTypes;
        }
    },
    actions:{
        fetchMiscellaneous : ({dispatch, commit, rootState},errorHandler) => {
            Api.prepareApiURL(miscApiURL);
            Api.fetch (
                (response) => {
                    //commit('SET_STATUS', response.body.status);
                    //commit('SET_TYPES', response.body.types);
                    commit('SET_MEAL_TYPES', response.body.mealTypes);
                    commit('SET_MEAL_PREFERENCES', response.body.mealPreferences);
                    commit('SET_MEAL_PREFERENCES', response.body.mealPreferences);
                    commit('SET_CHILD_AGES', response.body.childAges);
                    //commit('SET_CURRENCIES', response.body.currencies);
                    //commit('SET_AMENITIES', response.body.amenities);
                    //commit('SET_TIME_OF_DAY', response.body.timeOfDay);
                    //commit('SET_TOUR_GUIDE_OPTIONS', response.body.tourGuideOptions)
                },
                dispatch
            );
        },
    },
    getters:{
        types(state) {
            return state.types
        },
        currencies(state){
            return  state.currencies
        },
        status(state){
            return state.status
        },
        amenities(state) {
            return state.amenities
        },
        times(state) {
            return state.timeOfDay
        },
        tourGuideOptions(state) {
            return state.tourGuideOptions
        },
        weekDays(state) {
            return state.weekDays;
        },
        mealTypes(state) {
            return state.mealTypes;
        },
        mealPreferences(state) {
            return state.mealPreferences;
        },
        transferPointTypes(state) {
            return state.transferPointTypes;
        }
    }
};
