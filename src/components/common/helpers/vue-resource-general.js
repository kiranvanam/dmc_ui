
var Vue = require('vue');
import VueResource from 'vue-resource'
Vue.use(VueResource);

Vue.http.interceptors.push({

  response: function (response) {

    if( response.status == 401 ) {

      window.location = '/login';
    }

    return response;
  }

});

// Laravel CSRF protection
Vue.http.headers.common['X-CSRF-TOKEN'] = document.getElementById('csrf-token').getAttribute('value');
