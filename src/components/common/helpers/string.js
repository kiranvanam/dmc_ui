export function shorten(text, length) {
  if(text != null) {
    length = length == null? 50 : length
    return text.substr(0,length)
  }
}