import Dmc from '../../core/http-wrapper.js'

export default {
   state:{
     destinations: [],
     hotels:[],
     search : {
     },
   },
   mutations:{
    SET_DESTINATIONS(state, locations) {
      console.log(locations);
      state.destinations = locations;
    },
    SET_SEARCH(state, search) {
      state.search = search;
    },
    SET_HOTELS(state, hotels) {
      state.hotels = hotels;
    }
   },
   actions:{
      fetchDestinations: function({commit, dispatch}) {
        Dmc.prepareApiURL('/dmc-locations');

        Dmc.fetch(
          (response) => commit('SET_DESTINATIONS', response.body),
          dispatch
        );
      },
      serachForHotels ({commit, state, dispatch}, search) {
        Dmc.prepareApiURL('/search/hotels');
        commit('SET_SEARCH',search)
        Dmc.save(
          search,
          (response) => commit('SET_HOTELS', response.body.hotels),
          dispatch
        );
      }
   },
   getters:{
    destinations(state) {
      return state.destinations;
    },
    hotels(state) {
      return state.hotels;
    }
   }
}